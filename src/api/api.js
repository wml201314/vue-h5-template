const api = {
  Login: '/wxLogin', //微信授权登录
  UserInfo: '/getUserInfo', //获取信息
  UserName: '/name',
  getShare: '/getSignPackage', //获取分享
  getProblem: "/getProblem", //获取问题
  subProblem: "/subProblem", //提交积分
  getReport: '/getReport', //获取排行榜
  getRule: '/getRule', //获取规则
  getToken: '/getToken', //获取token
  getQiShu: '/getQiShu', //获取期数

}

export default api
